﻿using AutoMapper;
using CustomerRegistration.Core.Features.Registrations;
using CustomerRegistration.Persistence;
using CustomerRegistration.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using static CustomerRegistration.Core.Features.Registrations.MrGreenRegistration;

namespace CustomerRegistration.Core.Test.Unit.Features.Registrations
{
    public class MrGreenRegistrationHandler
    {
        private readonly MrGreenRegistration _sut;
        private readonly Context _ctx;

        public MrGreenRegistrationHandler()
        {
            var options = new DbContextOptionsBuilder<Context>()
                    .UseInMemoryDatabase(databaseName: "UntiTests")
                    .Options;

            _ctx = new Context(options);
            var mapperMq = new Mock<IMapper>();

            _sut = new MrGreenRegistration(_ctx, mapperMq.Object);
        }

        [Fact]
        public async Task MrGreenRegistration_WhenCustomerAlreadyExists_ThrowInvalidOperationException()
        {
            // Arrange
            var cmd = CreateValidCommand();

            var customer = new MrGreenCustomer
            {
                Brand = Brands.MrGreen,
                Email = cmd.Email,

            };

            _ctx.Add(customer);
            await _ctx.SaveChangesAsync();

            // Act
            var result = _sut.Handle(cmd, default);

            // Assert
            await Assert.ThrowsAsync<InvalidOperationException>(() => result);

        }

        private static Command CreateValidCommand() =>
          // TODO: use some data generator (e.g. Bogus)
          new Command
          {
              FirstName = "Max",
              LastName = "Planck",
              Street = "Groner Str.",
              Number = "h",
              ZipCode = "37001-37099",
              Email = "planck.max@nobel.com",
              PersonalNumber = "66260-704"
          };
    }
}
