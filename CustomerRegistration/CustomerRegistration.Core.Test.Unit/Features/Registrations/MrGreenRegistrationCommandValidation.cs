﻿using Shouldly;
using System.Linq;
using Xunit;
using static CustomerRegistration.Core.Features.Registrations.MrGreenRegistration;

namespace CustomerRegistration.Core.Test.Unit.Features.Registrations
{
    public class MrGreenRegistrationCommandValidation
    {
        private readonly CommandValidator _sut = new CommandValidator();

        [Fact]
        public void Command_WhenIsValid()
        {
            // Arrange
            var cmd = CreateValidCommand();
           
            // Act
            var result = _sut.Validate(cmd);

            // Assert
            result.IsValid.ShouldBeTrue();
            result.Errors.ShouldBeEmpty();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Command_WhenFirstNameIsNullOrEmpty_IsNotValid(string value)
        {
            // Arrange
            var cmd = CreateValidCommand();
            cmd.FirstName = value;

            // Act
            var result = _sut.Validate(cmd);

            // Assert
            result.IsValid.ShouldBeFalse();
            result.Errors.Any(x => x.PropertyName == nameof(Command.FirstName));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("55555-55a")]
        public void Command_WhenPersonalNumberHasWrongFormat_IsNotValid(string value)
        {
            // Arrange
            var cmd = CreateValidCommand();
            cmd.PersonalNumber = value;

            // Act
            var result = _sut.Validate(cmd);

            // Assert
            result.IsValid.ShouldBeFalse();
            result.Errors.Any(x => x.PropertyName == nameof(Command.PersonalNumber));
        }

        // TODO: add test for the rest of fields

        private static Command CreateValidCommand() =>
            // TODO: use some data generator (e.g. Bogus)
            new Command
            {
                FirstName = "Max",
                LastName = "Planck",
                Street = "Groner Str.",
                Number = "h",
                ZipCode = "37001-37099",
                Email = "planck.max@nobel.com",
                PersonalNumber = "66260-704"
            };
    }
}
