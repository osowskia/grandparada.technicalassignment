﻿using CustomerRegistration.Core.Features;
using CustomerRegistration.Dispatchers;
using CustomerRegistration.Dtos;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CustomerRegistration.Controllers
{
    [ApiController]
    [Route("api/customers")]
    public class CustomersController : ControllerBase
    {
        private readonly RegisterCommandFactory _commandFactory;
        private readonly IMediator _mediator;

        public CustomersController(RegisterCommandFactory commandFactory, IMediator mediator)
        {
            _commandFactory = commandFactory;
            _mediator = mediator;
        }

        [HttpPost("signup/{brand}")]
        public async Task<IActionResult> Register(string brand, RegisterInput data)
        {
            try
            {
                var cmd = _commandFactory.Create(brand, data);
                await _mediator.Send(cmd);
                return Ok();
            }
            catch (ValidationException ve)
            {
                return BadRequest(ve.Errors);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetCustomers()
        {
            var result = await _mediator.Send(new GetCustomers.Query());
            return Ok(result);
        }
    }

}
