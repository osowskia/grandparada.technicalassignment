﻿using CustomerRegistration.Core;
using CustomerRegistration.Core.Features.Registrations;
using CustomerRegistration.Dtos;
using MediatR;
using System;
using System.Text.Json;

namespace CustomerRegistration.Dispatchers
{
    public class RegisterCommandFactory
    {
        public IRequest Create(string brand, RegisterInput data)
        {
            return brand switch
            {
                Brands.MrGreen => CreateMrGreen(data),
                Brands.RedBet => CreateRedBet(data),
                _ => throw new ArgumentOutOfRangeException($"Brand {brand} is not supported."),
            };
        }

        private static IRequest CreateMrGreen(RegisterInput data)
        {

            var cmd = new MrGreenRegistration.Command
            {
                FirstName = data.FirstName,
                PersonalNumber = JsonSerializer.Deserialize<MrGreenAdditionalInfo>(data.AdditionalData?.GetRawText() ?? "{}")?.PersonalNumber
            };

            return cmd;
        }

        private static IRequest CreateRedBet(RegisterInput data)
        {
            var cmd = new RedBetRegistration.Command
            {
                FirstName = data.FirstName,
                FavFootballTeam = JsonSerializer.Deserialize<RedBetAdditianlInfo>(data.AdditionalData?.GetRawText() ?? "{}")?.FavFootballTeam
            };

            return cmd;
        }


        private class MrGreenAdditionalInfo
        {
            public string PersonalNumber { get; set; }
        }

        private class RedBetAdditianlInfo
        {
            public string FavFootballTeam { get; set; }
        }
    }
}
