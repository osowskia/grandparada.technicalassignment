﻿using System.Text.Json;

namespace CustomerRegistration.Dtos
{
    public class RegisterInput
    {
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Street { get; set; }
        public string Number { get; set; }
        public string ZipCode { get; set; }

        public JsonElement? AdditionalData { get; set; }
    }

}
