﻿namespace CustomerRegistration.Persistence.Entities
{
    public abstract class Customer
    {
        public int Id { get; set; }
        public string Brand { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Street { get; set; }
        public string Number { get; set; }
        public string ZipCode { get; set; }

    }

    public class MrGreenCustomer : Customer
    {
        public string PersonalNumber { get; set; }


    }

    public class RedBetCustomer : Customer
    {
        public string FavFootballTeam { get; set; }
    }
}
