﻿using CustomerRegistration.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerRegistration.Persistence.Configurations
{
    class CustomersConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Brand)
                    .IsRequired()
                    .HasMaxLength(64);
            builder.HasIndex(x => x.Brand);

            builder.Property(x => x.Email)
                    .IsRequired()
                    .HasMaxLength(128);

            builder.HasIndex(x => new { x.Email, x.Brand })
                    .IsUnique();

            builder.Property(x => x.FirstName)
                    .IsRequired();

            // TODO: add missing configuration 
        }
    }

    class MrGreenCustomersConfiguration : IEntityTypeConfiguration<MrGreenCustomer>
    {
        public void Configure(EntityTypeBuilder<MrGreenCustomer> builder)
        {
            builder.HasBaseType<Customer>();
        }
    }

    class RedBetCustomersConfiguration : IEntityTypeConfiguration<RedBetCustomer>
    {
        public void Configure(EntityTypeBuilder<RedBetCustomer> builder)
        {
            builder.HasBaseType<Customer>();
        }
    }

}
