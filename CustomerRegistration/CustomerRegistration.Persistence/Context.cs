﻿using CustomerRegistration.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;

namespace CustomerRegistration.Persistence
{
    public class Context : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<MrGreenCustomer> MrGreenCustomers { get; set; }
        public DbSet<RedBetCustomer> RedBetCustomers { get; set; }

        public Context(DbContextOptions<Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
