﻿using AutoMapper;
using CustomerRegistration.Persistence;
using CustomerRegistration.Persistence.Entities;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerRegistration.Core.Features.Registrations
{
    public abstract class BaseRegistration<T> where T : Customer
    {
        private readonly Context _context;
        private readonly IMapper _mapper;

        public BaseRegistration(Context context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        protected async Task<Unit> Handle(BaseCommand request, CancellationToken cancellationToken)
        {
            if (await _context.Set<T>().AnyAsync(x => x.Email == request.Email.ToLower(), cancellationToken))
                throw new InvalidOperationException("Customer with this e-mail already exists.");   // TODO: change exception to some result object

            var customer = _mapper.Map<T>(request);

            _context.Add(customer);
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        public abstract class BaseCommand : IRequest
        {
            public string Email { get; set; }

            public string FirstName { get; set; }
            public string LastName { get; set; }

            public string Street { get; set; }
            public string Number { get; set; }
            public string ZipCode { get; set; }

        }

        public abstract class BaseCommandValidator<TCommand> : AbstractValidator<TCommand>
            where TCommand : BaseCommand
        {
            public BaseCommandValidator()
            {
                RuleFor(x => x.Email).NotEmpty()
                                     .EmailAddress()
                                     .MaximumLength(128)
                                     ;
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.Street).NotEmpty();
                RuleFor(x => x.Number).NotEmpty();
                RuleFor(x => x.ZipCode).NotEmpty();

            }
        }

    }
}
