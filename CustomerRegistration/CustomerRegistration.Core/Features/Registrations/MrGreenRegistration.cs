﻿using AutoMapper;
using CustomerRegistration.Persistence;
using CustomerRegistration.Persistence.Entities;
using FluentValidation;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerRegistration.Core.Features.Registrations
{
    public class MrGreenRegistration : BaseRegistration<MrGreenCustomer>, IRequestHandler<MrGreenRegistration.Command>
    {
        public MrGreenRegistration(Context context, IMapper mapper) : base(context, mapper)
        {
        }

        public Task<Unit> Handle(Command request, CancellationToken cancellationToken)
        {
            return base.Handle(request, cancellationToken);
        }

        public class Command : BaseCommand, IRequest
        {
            public string PersonalNumber { get; set; }
        }

        public class MrGreenRegisterMapping : Profile
        {
            public MrGreenRegisterMapping()
            {
                CreateMap<BaseRegistration<MrGreenCustomer>.BaseCommand, Customer>()
                   .ForMember(x => x.Email, opt => opt.MapFrom(s => s.Email.ToLower()))
                   ;

                CreateMap<Command, MrGreenCustomer>()
                    .IncludeBase<BaseCommand, Customer>()
                    .ForMember(x => x.Brand, opt => opt.MapFrom(s => Brands.MrGreen))
                    ;
            }
        }

        public class CommandValidator : BaseCommandValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.PersonalNumber).NotEmpty().Matches(@"\d\d\d\d\d-\d\d\d");
            }
        }
    }
}
