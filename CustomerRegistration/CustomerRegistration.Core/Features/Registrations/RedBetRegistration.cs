﻿using AutoMapper;
using CustomerRegistration.Persistence;
using CustomerRegistration.Persistence.Entities;
using FluentValidation;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerRegistration.Core.Features.Registrations
{
    public class RedBetRegistration : BaseRegistration<RedBetCustomer>, IRequestHandler<RedBetRegistration.Command>
    {
        public RedBetRegistration(Context context, IMapper mapper) : base(context, mapper)
        {
        }

        public Task<Unit> Handle(Command request, CancellationToken cancellationToken)
        {
            return base.Handle(request, cancellationToken);
        }


        public class Command : BaseCommand, IRequest
        {
            public string FavFootballTeam { get; set; }
        }

        public class RedBetRegisterMapping : Profile
        {
            public RedBetRegisterMapping()
            {
                CreateMap<BaseRegistration<RedBetCustomer>.BaseCommand, Customer>()
                  .ForMember(x => x.Email, opt => opt.MapFrom(s => s.Email.ToLower()))
                  ;

                CreateMap<Command, RedBetCustomer>()
                    .IncludeBase<BaseCommand, Customer>()
                    .ForMember(x => x.Brand, opt => opt.MapFrom(s => Brands.RedBet))
                    ;
            }
        }

        public class CommandValidator : BaseCommandValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.FavFootballTeam).NotEmpty();
            }
        }
    }
}
