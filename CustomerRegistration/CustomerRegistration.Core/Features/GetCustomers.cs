﻿using CustomerRegistration.Persistence;
using CustomerRegistration.Persistence.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerRegistration.Core.Features
{
    public class GetCustomers : IRequestHandler<GetCustomers.Query, List<GetCustomers.CustomerInfo>>
    {
        private readonly Context _context;

        public GetCustomers(Context context)
        {
            _context = context;
        }

        public Task<List<CustomerInfo>> Handle(Query request, CancellationToken cancellationToken)
        {
            return _context.Customers.Select(ProjectToCustomerInfo).ToListAsync();
        }


        private static readonly Expression<Func<Customer, CustomerInfo>> ProjectToCustomerInfo = e => new CustomerInfo
        {
            Brand = e.Brand,
            FirstName = e.FirstName,
            LastName = e.LastName,
            Email = e.Email
        };


        public struct Query : IRequest<List<CustomerInfo>>
        {
        }

        public class CustomerInfo
        {
            public string Brand { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
        }
    }
}
